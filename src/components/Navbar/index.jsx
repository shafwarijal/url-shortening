// import Top from '@components/Top';
// import List from '@components/List';
// import Bottom from '@components/Bottom';
// import NightsStayIcon from '@mui/icons-material/NightsStay';
import CloseIcon from '@mui/icons-material/Close';
// import WbSunnyIcon from '@mui/icons-material/WbSunny';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { FormattedMessage } from 'react-intl';
import { useState } from 'react';

import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect, useDispatch } from 'react-redux';
// import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
// import { setTheme } from '@containers/Settheme/actions';
import { setMenuToggle } from '@containers/Shortlink/actions';
import { selectToggle } from '@containers/Shortlink/selector';
import { selectLocale } from '@containers/App/selectors';
import { setLocale } from '@containers/App/actions';
import LanguageIcon from '@mui/icons-material/Language';
import Theme from '@components/Theme';

import FlagId from '@static/images/flags/id.png';
import FlagEn from '@static/images/flags/en.png';

import classes from './style.module.scss';

const Navbar = ({ toggle, locale }) => {
  const dispatch = useDispatch();
  const [menuPosition, setMenuPosition] = useState(null);
  const open = Boolean(menuPosition);

  const handleClick = (event) => {
    setMenuPosition(event.currentTarget);
  };

  const handleToggle = () => {
    if (toggle === false) {
      dispatch(setMenuToggle(true));
    } else {
      dispatch(setMenuToggle(false));
    }
  };

  const handleClose = () => {
    setMenuPosition(null);
  };

  const onSelectLang = (lang) => {
    if (lang !== locale) {
      dispatch(setLocale(lang));
    }
    handleClose();
  };

  return (
    <>
      <div className={classes.navbar}>
        <div className={classes.addPadding}>
          <div className={classes.logo}>Shortly</div>
          <div className={classes.menuLink}>
            <ul className={classes.menuLinkLeft}>
              <li className={classes.listMenuLeft}>
                <a href="/">
                  <FormattedMessage id="nav_feature" />
                </a>
              </li>
              <li className={classes.listMenuLeft}>
                <a href="/">
                  <FormattedMessage id="nav_price" />
                </a>
              </li>
              <li className={classes.listMenuLeft}>
                <a href="/">
                  <FormattedMessage id="nav_resource" />
                </a>
              </li>
            </ul>
            <div className={classes.boxMenuRight}>
              <ul className={classes.menuLinkRight}>
                <li className={classes.listMenuRight}>
                  <a href="/">
                    <FormattedMessage id="nav_login" />
                  </a>
                </li>
                <li className={classes.listMenuRight}>
                  <a href="/" className={classes.signup}>
                    <FormattedMessage id="nav_signup" />
                  </a>
                </li>
              </ul>
              <div className={classes.boxIconMenuRight}>
                <IconButton sx={{ p: '10p' }} className={classes.btn} variant="contained" onClick={handleClick}>
                  <LanguageIcon sx={{ fontSize: 30, color: 'grey' }} />
                </IconButton>
                <Theme />
              </div>

              <Menu open={open} anchorEl={menuPosition} onClose={handleClose}>
                <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
                  <div className={classes.boxMenuLang}>
                    <Avatar className={classes.menuAvatar} src={FlagId} />
                    <div className={classes.menuLang}>
                      <FormattedMessage id="app_lang_id" />
                    </div>
                  </div>
                </MenuItem>
                <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
                  <div className={classes.boxMenuLang}>
                    <Avatar className={classes.menuAvatar} src={FlagEn} />
                    <div className={classes.menuLang}>
                      <FormattedMessage id="app_lang_en" />
                    </div>
                  </div>
                </MenuItem>
              </Menu>
            </div>
          </div>
          <div className={classes.menuToggle}>
            <IconButton sx={{ p: '10px' }} className={classes.btn} variant="contained" onClick={handleClick}>
              <LanguageIcon sx={{ fontSize: 30, color: 'grey' }} />
              {/* <ExpandMoreIcon /> */}
            </IconButton>
            <Theme />
            <IconButton className={classes.btn} variant="contained" onClick={handleToggle}>
              {toggle === false ? (
                <MenuIcon sx={{ fontSize: 30, color: 'grey' }} />
              ) : (
                <CloseIcon sx={{ fontSize: 30, color: 'grey' }} />
              )}
            </IconButton>
          </div>
        </div>
      </div>
      <div className={`${classes.menuMobile} ${toggle === false ? '' : classes.activeMenuMobile}`}>
        <div className={classes.menuMobileContainer}>
          <ul className={classes.menuLinkMobile}>
            <li className={classes.listMenuLeft}>
              <a href="/">
                <FormattedMessage id="nav_feature" />
              </a>
            </li>
            <li className={classes.listMenuLeft}>
              <a href="/">
                <FormattedMessage id="nav_price" />
              </a>
            </li>
            <li className={classes.listMenuLeft}>
              <a href="/">
                <FormattedMessage id="nav_resource" />
              </a>
            </li>
            <li className={classes.listMenuRight}>
              <a href="/">
                <FormattedMessage id="nav_login" />
              </a>
            </li>
            <li className={classes.listMenuRight}>
              <a href="/" className={classes.signup}>
                <FormattedMessage id="nav_signup" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

Navbar.propTypes = {
  toggle: PropTypes.bool,
  locale: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  toggle: selectToggle,
  locale: selectLocale,
});

export default connect(mapStateToProps)(Navbar);
