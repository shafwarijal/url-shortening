import imageHero from '@static/images/illustration-working.svg';
import { FormattedMessage } from 'react-intl';

import classes from './style.module.scss';

const Hero = () => (
  <div className={classes.heroContainer}>
    <div className={classes.hero}>
      <div className={classes.heroLeft}>
        <div className={classes.text1}>
          <FormattedMessage id="hero_title" />
        </div>
        <div className={classes.text2}>
          <FormattedMessage id="hero_subtitle" />
        </div>
        <div className={classes.btnHero}>
          <a href="/">
            <FormattedMessage id="btn_getstarted" />
          </a>
        </div>
        {/* <Button className={classes.btnHero}>Get Started</Button> */}
      </div>
      <div className={classes.heroRight}>
        <img src={imageHero} alt="" />
      </div>
    </div>
  </div>
);

export default Hero;
