import PropTypes from 'prop-types';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Button from '@mui/material/Button';
import iconBrand from '@static/images/icon-brand-recognition.svg';
import iconDetailed from '@static/images/icon-detailed-records.svg';
import iconFully from '@static/images/icon-fully-customizable.svg';
import { useDispatch, connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectShortUrl, selectShortUrlLoadinng, selectShortUrlError } from '@containers/ShortUrl/selectors';
import { getShortUrl, setShortUrlRemove } from '@containers/ShortUrl/actions';
import { useState } from 'react';
import { useClipboard } from 'use-clipboard-copy';
import { FormattedMessage, useIntl } from 'react-intl';
import { IconButton } from '@mui/material';
import Loader from '@components/Loader';
import DeleteIcon from '@mui/icons-material/Delete';

// import { selectLocale } from '@containers/App/selectors';
import classes from './style.module.scss';

const Statistic = ({ shortUrl, shortUrlLoading, shortUrlError }) => {
  const dispatch = useDispatch();
  const clipboard = useClipboard();
  const intl = useIntl();

  const [value, setValue] = useState('');
  const [copied, setCopied] = useState(false);
  const [id, setId] = useState('');

  // const sadad = 'asdsa';
  const handleInputUrlShort = (event) => {
    // event.preventDefault();
    setValue(event.target.value);
  };

  const handleSubmitUrlShort = (e) => {
    e.preventDefault();

    dispatch(getShortUrl(value));
    setValue('');
  };

  const handleCopy = (shortLink, idLink) => {
    clipboard.copy(shortLink);
    setCopied(true);
    setId(idLink);
    const timer = setInterval(() => {
      setCopied(false);
      clearInterval(timer);
    }, 1600);
  };

  const handleRemove = (code) => {
    const remove = shortUrl.filter((short) => short.code !== code);

    dispatch(setShortUrlRemove(remove));
  };
  // console.log(shortUrlError);

  const errorMessage = () => {
    switch (shortUrlError.errorCode) {
      case 1:
        return intl.formatMessage({ id: 'error_msg_1' });
      case 2:
        return intl.formatMessage({ id: 'error_msg_2' });
      case 3:
        return intl.formatMessage({ id: 'error_msg_3' });
      case 4:
        return intl.formatMessage({ id: 'error_msg_4' });
      case 5:
        return intl.formatMessage({ id: 'error_msg_5' });
      case 6:
        return intl.formatMessage({ id: 'error_msg_6' });
      case 7:
        return intl.formatMessage({ id: 'error_msg_7' });
      case 8:
        return intl.formatMessage({ id: 'error_msg_8' });
      case 9:
        return intl.formatMessage({ id: 'error_msg_9' });
      case 10:
        return intl.formatMessage({ id: 'error_msg_10' });
      case 11:
        return intl.formatMessage({ id: 'error_msg_11' });
      default:
        return intl.formatMessage({ id: 'error_default' });
    }
  };

  return (
    <>
      <div className={classes.statisticContainer}>
        <div className={classes.statistic}>
          <div className={classes.shortlinkBox}>
            <Paper
              component="form"
              sx={{ p: '0.8rem 1rem', display: 'flex', alignItems: 'center', width: '100%' }}
              className={classes.paper}
              onSubmit={handleSubmitUrlShort}
            >
              <InputBase
                sx={{ ml: 1, flex: 1, fontFamily: `inherit` }}
                placeholder={intl.formatMessage({ id: 'statistic_placeholder' })}
                inputProps={{ 'aria-label': 'search google maps' }}
                value={value}
                onChange={handleInputUrlShort}
              />
            </Paper>
            <Button
              sx={{
                p: '0 4rem',
                textTransform: 'none',
                fontFamily: `inherit`,
                whiteSpace: 'nowrap',
                fontWeight: '700',
                background: 'var(--color-button)',
                color: 'var(--color-text-in-btn)',
                '&:hover': {
                  backgroundColor: 'hsl(180, 66%, 41%)',
                },
              }}
              variant="contained"
              color="inherit"
              className={classes.shortBtn}
              onClick={handleSubmitUrlShort}
            >
              <FormattedMessage id="btn_shorten" />
            </Button>
          </div>
          {shortUrlError !== null ? <div className={classes.errorBox}>Error - {errorMessage()}</div> : ''}
          <Loader isLoading={shortUrlLoading} />
          {shortUrl?.map((short) => (
            <div className={classes.boxResultShort} key={short.code}>
              <div className={classes.inputLink}>{short.original_link}</div>

              <div className={classes.resultLink}>
                <div className={classes.result}>{short.full_short_link}</div>
                <div className={classes.boxIconResult}>
                  <Button
                    sx={{
                      p: '0.6rem 2rem',
                      textTransform: 'none',
                      fontFamily: `inherit`,
                      whiteSpace: 'nowrap',
                      fontWeight: '700',
                      background: 'var(--color-button)',
                      color: 'var(--color-text-in-btn)',
                      '&:hover': {
                        backgroundColor: 'hsl(180, 66%, 41%)',
                      },
                    }}
                    variant="contained"
                    color="inherit"
                    className={`${classes.resultLinkBtn} ${
                      id === short.code && copied ? classes.resultLinkBtnCopied : ''
                    }`}
                    onClick={() => handleCopy(short.full_short_link, short.code)}
                  >
                    {id === short.code && copied ? <FormattedMessage id="copied" /> : <FormattedMessage id="copy" />}
                  </Button>
                  <IconButton onClick={() => handleRemove(short.code)}>
                    <DeleteIcon sx={{ fontSize: 30 }} />
                  </IconButton>
                </div>
              </div>
            </div>
          ))}
          <div className={classes.titleStatistic}>
            <FormattedMessage id="statistic_title" />
          </div>
          <div className={classes.subTitleStatistic}>
            <FormattedMessage id="statistic_subtite" />
          </div>
          <div className={classes.boxCard}>
            <div className={classes.card}>
              <div className={classes.iconCard}>
                <img className={classes.iconCardSvg} src={iconBrand} alt="" />
              </div>
              <div className={classes.titleCard}>
                <FormattedMessage id="card1_title" />
              </div>
              <div className={classes.subTitleCard}>
                <FormattedMessage id="card1_subtitle" />
              </div>
            </div>
            <div className={classes.card}>
              <div className={classes.iconCard}>
                <img className={classes.iconCardSvg} src={iconDetailed} alt="" />
              </div>
              <div className={classes.titleCard}>
                <FormattedMessage id="card2_title" />
              </div>
              <div className={classes.subTitleCard}>
                <FormattedMessage id="card2_subtitle" />
              </div>
            </div>
            <div className={classes.card}>
              <div className={classes.iconCard}>
                <img className={classes.iconCardSvg} src={iconFully} alt="" />
              </div>
              <div className={classes.titleCard}>
                <FormattedMessage id="card3_title" />
              </div>
              <div className={classes.subTitleCard}>
                <FormattedMessage id="card3_subtitle" />
              </div>
            </div>

            <div className={classes.line} />
            <div className={classes.lineVertical} />
          </div>
        </div>
      </div>
      <div className={classes.statisticFooterContainer}>
        <div className={classes.statisticFooter}>
          <div className={classes.titleStatisticFooter}>
            <FormattedMessage id="statistic_footertitle" />
          </div>
          <div className={classes.btnStatisticFooter}>
            <a href="_blank">
              <FormattedMessage id="btn_getstarted" />
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

Statistic.propTypes = {
  shortUrl: PropTypes.array,
  shortUrlLoading: PropTypes.bool,
  shortUrlError: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  shortUrl: selectShortUrl,
  shortUrlLoading: selectShortUrlLoadinng,
  shortUrlError: selectShortUrlError,
});

export default connect(mapStateToProps)(Statistic);
