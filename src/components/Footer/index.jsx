import iconInstagram from '@static/images/icon-instagram.svg';
import iconFacebook from '@static/images/icon-facebook.svg';
import iconTwitter from '@static/images/icon-twitter.svg';
import iconPinterest from '@static/images/icon-pinterest.svg';
import { FormattedMessage } from 'react-intl';

import classes from './style.module.scss';

const Footer = () => (
  <div className={classes.footerContainer}>
    <div className={classes.footer}>
      <div className={classes.logoFooter}>Shortly</div>
      <div className={classes.menu}>
        <div className={classes.features}>
          <div className={classes.titleFeatures}>
            <FormattedMessage id="nav_feature" />
          </div>
          <ul className={classes.listMenuFeatures}>
            <li>
              <a href="/">
                <FormattedMessage id="footer_link_short" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_branded" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_analytic" />
              </a>
            </li>
          </ul>
        </div>
        <div className={classes.resources}>
          <div className={classes.titleResources}>
            <FormattedMessage id="nav_resource" />
          </div>
          <ul className={classes.listMenuResources}>
            <li>
              <a href="/">
                <FormattedMessage id="footer_blog" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_developer" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_support" />
              </a>
            </li>
          </ul>
        </div>
        <div className={classes.company}>
          <div className={classes.titleCompany}>
            <FormattedMessage id="footer_company" />
          </div>
          <ul className={classes.listMenuCompany}>
            <li>
              <a href="/">
                <FormattedMessage id="footer_about" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_team" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_careers" />
              </a>
            </li>
            <li>
              <a href="/">
                <FormattedMessage id="footer_contact" />
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className={classes.social}>
        <ul className={classes.listSocial}>
          <li>
            <img src={iconFacebook} alt="" />
          </li>
          <li>
            <img src={iconTwitter} alt="" />
          </li>
          <li>
            <img src={iconPinterest} alt="" />
          </li>
          <li>
            <img src={iconInstagram} alt="" />
          </li>
        </ul>
      </div>
    </div>
  </div>
);

export default Footer;
