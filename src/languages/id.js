export default {
  nav_feature: 'Fitur',
  nav_price: 'Harga',
  nav_resource: 'Sumber Daya',
  nav_login: 'Masuk',
  nav_signup: 'Daftar',

  hero_title: 'Lebih dari sekadar tautan yang lebih pendek',
  hero_subtitle: `Bangun pengenalan merek Anda dan dapatkan wawasan terperinci tentang kinerja tautan Anda.`,

  btn_getstarted: 'Mulai Sekarang',
  btn_shorten: 'Pendekkan',

  statistic_placeholder: 'Pendekkan tautan di sini . . .',

  copy: 'Salin',
  copied: 'Disalin!',

  statistic_title: 'Statistik Lanjutan',
  statistic_subtite: 'Lacak kinerja tautan Anda di seluruh web dengan dasbor statistik lanjutan kami.',

  card1_title: 'Pengenalan Merek',
  card1_subtitle: `Tingkatkan pengenalan merek Anda dengan setiap klik. Tautan generik tidak berarti. Tautan merek membantu membangun kepercayaan pada konten Anda.`,

  card2_title: 'Rekaman Rinci',
  card2_subtitle: `Dapatkan wawasan tentang siapa yang mengklik tautan Anda. Mengetahui kapan dan di mana orang terlibat dengan konten Anda membantu pengambilan keputusan yang lebih baik.`,

  card3_title: 'Sepenuhnya Dapat Dikustomisasi',
  card3_subtitle: `Tingkatkan kesadaran merek dan penemuan konten melalui tautan yang dapat disesuaikan, meningkatkan keterlibatan audiens.`,

  statistic_footertitle: 'Tingkatkan tautan Anda hari ini',

  footer_link_short: 'Pendekkan Tautan',
  footer_branded: 'Tautan Bermerk',
  footer_analytic: 'Analitik',

  footer_blog: 'Blog',
  footer_developer: 'Pengembang',
  footer_support: 'Dukungan',

  footer_company: 'Perusahaan',
  footer_about: 'Tentang',
  footer_team: 'Tim Kami',
  footer_careers: 'Karir',
  footer_contact: 'Kontak',

  error_msg_1: 'Tidak ada URL yang ditentukan (parameter "url" kosong)',
  error_msg_2: 'URL yang diserahkan tidak valid',
  error_msg_3: 'Batas permintaan tercapai. Tunggu sebentar dan coba lagi',
  error_msg_4: 'Alamat IP telah diblokir karena melanggar ketentuan layanan kami',
  error_msg_5: 'Kode shrtcode (slug) sudah digunakan',
  error_msg_6: 'Kesalahan tidak dikenal',
  error_msg_7: 'Tidak ada kode yang ditentukan (parameter "code" kosong)',
  error_msg_8: 'Kode yang diserahkan tidak valid (kode tidak ditemukan/tautan pendek tersebut tidak ada)',
  error_msg_9: 'Parameter yang diperlukan hilang',
  error_msg_10:
    'Mencoba untuk memendekkan Tautan yang tidak diizinkan. Informasi lebih lanjut tentang tautan yang tidak diizinkan',
  error_msg_11: 'URL Sudah ada',

  app_greeting: 'Hai dari Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Halaman tidak ditemukan',
  app_lang_id: 'Bahasa Indonesia',
  app_lang_en: 'Bahasa Inggris',
};
