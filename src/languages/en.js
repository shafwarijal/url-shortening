export default {
  nav_feature: 'Features',
  nav_price: 'Pricing',
  nav_resource: 'Resources',
  nav_login: 'Login',
  nav_signup: 'Sign Up',

  hero_title: 'More than just shorter links',
  hero_subtitle: `Build your brand's recognition and get detailed insights on how your links are performing.`,

  btn_getstarted: 'Get Started',
  btn_shorten: 'Shorten it',

  statistic_placeholder: 'Shorten a link here . . .',

  copy: 'Copy',
  copied: 'Copied!',

  statistic_title: 'Advanced Statistics',
  statistic_subtite: 'Track how your links are performing across the web with our advanced statistics dashboard',

  card1_title: 'Brand Recognation',
  card1_subtitle: `Boost your brand recognation with each click. Generic links don't mean a thing. Branded links help instill confidence in your content.`,

  card2_title: 'Detailed Records',
  card2_subtitle: `Gain insights into who is clicking your links. Knowing when and where people engange with your content helps inform better decisions.`,

  card3_title: 'Fully Customizable',
  card3_subtitle: `Improve brand awareness and content discovearbility through customizable links, supercharging audience engangement.`,

  statistic_footertitle: 'Boost your links today',

  footer_link_short: 'Link Shortening',
  footer_branded: 'Branded Links',
  footer_analytic: 'Analytics',

  footer_blog: 'Blog',
  footer_developer: 'Developers',
  footer_support: 'Support',

  footer_company: 'Company',
  footer_about: 'About',
  footer_team: 'Our Team',
  footer_careers: 'Careers',
  footer_contact: 'Contact',

  error_msg_1: 'No URL specified ("url" parameter is empty)',
  error_msg_2: 'Invalid URL submitted',
  error_msg_3: 'Rate limit reached. Wait a second and try again',
  error_msg_4: 'IP-Address has been blocked because of violating our terms of service',
  error_msg_5: 'shrtcode code (slug) already taken/in use',
  error_msg_6: 'Unknown error',
  error_msg_7: 'No code specified ("code" parameter is empty)',
  error_msg_8: 'Invalid code submitted (code not found/there is no such short-link)',
  error_msg_9: 'Missing required parameters',
  error_msg_10: 'Trying to shorten a disallowed Link. More information on disallowed links',
  error_msg_11: 'URL already exist',

  app_greeting: 'Hi from Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Page not found',
  app_lang_id: 'Indonesian',
  app_lang_en: 'English',
};
