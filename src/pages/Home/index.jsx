import Navbar from '@components/Navbar';
import Hero from '@components/Hero';
import Statistic from '@components/Statistic';
import Footer from '@components/Footer';

import PropTypes from 'prop-types';
import { selectTheme } from '@containers/Settheme/selector';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import classes from './style.module.scss';

const Home = ({ theme }) => {
  const isDark = theme === 'dark';

  return (
    <div className={isDark ? classes.darkTheme : classes.lightTheme}>
      <Navbar />
      <Hero />
      <Statistic />
      <Footer />
    </div>
  );
};

Home.propTypes = {
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

export default connect(mapStateToProps)(Home);
