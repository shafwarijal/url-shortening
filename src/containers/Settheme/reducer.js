import { produce } from 'immer';

import { SET_THEME } from '@containers/Settheme/constants';

export const initialState = {
  theme: 'light',
};

export const storedKey = ['theme'];

// eslint-disable-next-line default-param-last
const themeReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_THEME:
        draft.theme = action.theme;
        break;
      default:
        break;
    }
  });

export default themeReducer;
