import { produce } from 'immer';
import { SET_ADVICE, SET_ADVICE_ERROR, SET_ADVICE_LOADING } from './constants';

export const initialState = {
  advice: {},
  adviceError: null,
  adviceLoading: false,
};

// eslint-disable-next-line default-param-last
const adviceReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_ADVICE:
        draft.advice = action.advice;
        break;
      case SET_ADVICE_ERROR:
        draft.adviceerror = action.adviceerror;
        break;
      case SET_ADVICE_LOADING:
        draft.adviceloading = action.adviceloading;
        break;
      default:
        break;
    }
  });

export default adviceReducer;
