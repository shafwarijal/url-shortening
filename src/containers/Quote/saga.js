import { takeLatest, call, put } from 'redux-saga/effects';

import { getAdviceApi } from '@domain/api';
import { setAdviceApi, setAdviceError, setAdviceLoading } from './actions';
import { GET_ADVICE } from './constants';

export function* doGetAdviceApi() {
  yield put(setAdviceLoading(true));
  yield put(setAdviceError(null));
  try {
    const adviceApi = yield call(getAdviceApi);

    yield put(setAdviceApi(adviceApi.slip));
    yield put(setAdviceError(null));

    // console.log(adviceApi);
  } catch (error) {
    yield put(setAdviceError(error));
    // console.error(error);
  }
  yield put(setAdviceLoading(false));
}
export default function* adviceSaga() {
  yield takeLatest(GET_ADVICE, doGetAdviceApi);
}
