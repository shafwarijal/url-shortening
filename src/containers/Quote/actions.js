import { GET_ADVICE, SET_ADVICE, SET_ADVICE_ERROR, SET_ADVICE_LOADING } from './constants';

export const getAdviceApi = () => ({
  type: GET_ADVICE,
});

export const setAdviceApi = (advice) => ({
  type: SET_ADVICE,
  advice,
});

export const setAdviceError = (adviceerror) => ({
  type: SET_ADVICE_ERROR,
  adviceerror,
});

export const setAdviceLoading = (adviceloading) => ({
  type: SET_ADVICE_LOADING,
  adviceloading,
});
