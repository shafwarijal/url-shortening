import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAdviceState = (state) => state.setadvice || initialState;

const selectAdvice = createSelector(selectAdviceState, (state) => state.advice);
const selectAdviceError = createSelector(selectAdviceState, (state) => state.adviceerror);
const selectAdviceLoading = createSelector(selectAdviceState, (state) => state.adviceloading);

export { selectAdvice, selectAdviceError, selectAdviceLoading };
