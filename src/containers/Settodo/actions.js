import {
  SET_TODO,
  SET_TODO_FILTER,
  SET_TODO_REMOVE,
  SET_TODO_ACTIVE,
  SET_TODO_CLEAR,
  SET_TODO_EDIT,
  SET_TODO_ISEDITING,
} from './constants';

export const setTodo = (todos) => ({
  type: SET_TODO,
  todos,
});

export const setTodoFilter = (todofilter) => ({
  type: SET_TODO_FILTER,
  todofilter,
});

export const setTodoRemove = (todoremove) => ({
  type: SET_TODO_REMOVE,
  todoremove,
});

export const setTodoActive = (todoactive) => ({
  type: SET_TODO_ACTIVE,
  todoactive,
});

export const setTodoClear = (todoclear) => ({
  type: SET_TODO_CLEAR,
  todoclear,
});

export const setTodoIsediting = (todoisediting) => ({
  type: SET_TODO_ISEDITING,
  todoisediting,
});

// export const setTodoEditValue = (getvalueedit) => ({
//   type: T
// })

export const setTodoEdit = (todoedit) => ({
  type: SET_TODO_EDIT,
  todoedit,
});
