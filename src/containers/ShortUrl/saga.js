import { takeLatest, call, put, select } from 'redux-saga/effects';

import { getShortUrl } from '@domain/api';
import { setShortUrl, setShortUrlLoading, setShortUrlError } from '@containers/ShortUrl/actions';
import { GET_SHORT_URL } from '@containers/ShortUrl/constants';
import { selectShortUrl } from './selectors';

export function* doGetShortUrl({ url }) {
  yield put(setShortUrlLoading(true));
  try {
    const arr = yield select(selectShortUrl);
    if (url !== '' && arr.map((item) => item.original_link).find((link) => link.includes(url))) {
      yield put(setShortUrlError({ errorCode: 11 }));

      yield put(setShortUrlLoading(false));

      return -1;
    }

    const shortUrl = yield call(getShortUrl, url);
    if (shortUrl?.result) {
      yield put(setShortUrlError(null));
      yield put(setShortUrl(shortUrl.result));
    }
  } catch (err) {
    const { response } = err;
    const error = response?.data?.error;
    const errorCode = response?.data?.error_code;
    console.log(errorCode);
    yield put(setShortUrlError({ error, errorCode }));
  }
  yield put(setShortUrlLoading(false));
}

export default function* shortUrlSaga() {
  yield takeLatest(GET_SHORT_URL, doGetShortUrl);
}
