import { produce } from 'immer';

import {
  SET_SHORT_URL,
  SET_SHORT_URL_LOADING,
  SET_SHORT_URL_ERROR,
  SET_SHORT_URL_REMOVE,
} from '@containers/ShortUrl/constants';

export const initialState = {
  shortUrl: [],
  shortUrlLoading: false,
  shortUrlError: null,
};

export const storedKey = ['shortUrl'];

// eslint-disable-next-line default-param-last
const shortUrlReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_SHORT_URL:
        draft.shortUrl = [...draft.shortUrl, action.shortUrl];
        break;
      case SET_SHORT_URL_LOADING:
        draft.shortUrlLoading = action.shortUrlLoading;
        break;
      case SET_SHORT_URL_ERROR:
        draft.shortUrlError = action.shortUrlError;
        break;
      case SET_SHORT_URL_REMOVE:
        draft.shortUrl = action.shortUrlRemove;
        break;
      default:
        break;
    }
  });

export default shortUrlReducer;
