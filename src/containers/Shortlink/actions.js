import { SET_MENU_TOGGLE } from './constants';

export const setMenuToggle = (toggle) => ({
  type: SET_MENU_TOGGLE,
  toggle,
});
