import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectShortlinkState = (state) => state.setshortlink || initialState;

const selectToggle = createSelector(selectShortlinkState, (state) => state.toggle);

export { selectToggle };
