import { produce } from 'immer';

import { SET_MENU_TOGGLE } from './constants';

export const initialState = {
  toggle: false,
};

// eslint-disable-next-line default-param-last
const shortlinkReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_MENU_TOGGLE:
        draft.toggle = action.toggle;
        break;
      default:
        break;
    }
  });

export default shortlinkReducer;
