import { combineReducers } from 'redux';
import themeReducer, { storedKey as storedThemeState } from '@containers/Settheme/reducer';
import todoReducer, { storedKey as storedTodoState } from '@containers/Settodo/reducer';
import adviceReducer from '@containers/Quote/reducer';
import shortlinkReducer from '@containers/Shortlink/reducer';
import appReducer from '@containers/App/reducer';
import languageReducer from '@containers/Language/reducer';
import shortUrlReducer, { storedKey as storedShortState } from '@containers/ShortUrl/reducer';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  settheme: { reducer: themeReducer, whitelist: storedThemeState },
  settodo: { reducer: todoReducer, whitelist: storedTodoState },
  shortUrl: { reducer: shortUrlReducer, whitelist: storedShortState },
};

const temporaryReducers = {
  language: languageReducer,
  setadvice: adviceReducer,
  app: appReducer,
  setshortlink: shortlinkReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
